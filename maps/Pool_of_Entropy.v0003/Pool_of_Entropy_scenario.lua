version = 3 -- Lua Version. Dont touch this
ScenarioInfo = {
    name = "Pool of Entropy",
    description = "A rogue system is still launching warheads while the infinite war has been over for years. \r\n\r\nMap made by (Jip) Willem Wijnia. \r\nMap made for EntropyWins\r\n\r\nYou can find the latest version at: https://gitlab.com/supreme-commander-forged-alliance/maps",
    preview = '',
    map_version = 3,
    type = 'skirmish',
    starts = true,
    size = {512, 512},
    reclaim = {153412, 5875},
    map = '/maps/Pool_of_Entropy.v0003/Pool_of_Entropy.scmap',
    save = '/maps/Pool_of_Entropy.v0003/Pool_of_Entropy_save.lua',
    script = '/maps/Pool_of_Entropy.v0003/Pool_of_Entropy_script.lua',
    norushradius = 40,
    Configurations = {
        ['standard'] = {
            teams = {
                {
                    name = 'FFA',
                    armies = {'ARMY_1', 'ARMY_2', 'ARMY_3', 'ARMY_4', 'ARMY_5'}
                },
            },
            customprops = {
                ['ExtraArmies'] = STRING( 'ARMY_17 NEUTRAL_CIVILIAN' ),
            },
        },
    },
}
