local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local ScenarioFramework = import('/lua/ScenarioFramework.lua')
local DefaultExplosions = import('/lua/defaultexplosions.lua')

function OnPopulate()
    ScenarioUtils.InitializeArmies()
    -- ScenarioFramework.SetPlayableArea('AREA_1' , false)
end

function OnStart(scenario)

    -- make the neutral civilians ignore the playable area
    SetIgnorePlayableRect("NEUTRAL_CIVILIAN", true);

    -- create the units in the center
    local units = ScenarioUtils.CreateArmyGroup("NEUTRAL_CIVILIAN", "CENTER")
    local commanders = EntityCategoryFilterDown(categories.ENGINEER, units)
    local launchers = EntityCategoryFilterDown(categories.SILO, units)
    local shields = EntityCategoryFilterDown(categories.SHIELD, units)

    -- make it all immune
    MakeImmune(units)

    -- make them support commanders assist
    AssistNearestLauncher(commanders, launchers)

    -- have intel over the shields and adjust them slightly
    SetIntel(shields, 'Vision')
    SetShields(shields)

    -- empty them every now and then 
    ForkThread(StrategicAssistThread, launchers)

    -- make them launch every now and then
    if ScenarioInfo.Options.PoolOfEntropyRogueSystem then 
        if ScenarioInfo.Options.PoolOfEntropyRogueSystem == 'online' then 
            ForkThread(StrategicThread, launchers)
            AdjustProjectileTarget(launchers)
        end 
    end

    -- destroy units that get too close to the shields
    local strArea = "CENTER"
    local depth = 37
    ForkThread(DestroyUnitsThread, strArea, depth)
end

function MakeImmune(units)
    for k, unit in units do 
        unit:SetReclaimable(false);
        unit:SetCapturable(false);
        unit:SetDoNotTarget(true);
        unit:SetRegenRate(5000);
        --unit:SetUnSelectable(true);
        unit:SetCanTakeDamage(false);
        unit:SetCanBeKilled(false);
    end
end

function AdjustProjectileTarget(launchers, tracker)

    local size = ScenarioInfo.size
    local hsize = { size[1] * 0.5, size[2] * 0.5 }
    local CreateProjectileAtMuzzle = function(self, muzzle)

        local proj = self:CreateProjectileForWeapon(muzzle)

        -- INJECTED CODE --

        -- original target
        local target = proj:GetCurrentTargetPosition()

        -- compute the direction from the map center
        local direction = VECTOR3(target[1] - hsize[1], 20, target[3] - hsize[2])

        -- offset the target slightly so that it is outisde the map
        local offset = 1.3
        target = VECTOR3(direction[1] * offset + hsize[1], 20, direction[3] * offset + hsize[2])

        -- set the new target
        proj:SetNewTargetGround(target)

        -- END OF INJECTED CODE --

        if not proj or proj:BeenDestroyed() then
            return proj
        end

        local bp = self:GetBlueprint()
        if bp.DetonatesAtTargetHeight == true then
            local pos = self:GetCurrentTargetPos()
            if pos then
                local theight = GetSurfaceHeight(pos[1], pos[3])
                local hght = pos[2] - theight
                proj:ChangeDetonateAboveHeight(hght)
            end
        end

        if bp.Flare then
            proj:AddFlare(bp.Flare)
        end
        if self.unit:GetCurrentLayer() == 'Water' and bp.Audio.FireUnderWater then
            self:PlaySound(bp.Audio.FireUnderWater)
        elseif bp.Audio.Fire then
            self:PlaySound(bp.Audio.Fire)
        end

        self:CheckBallisticAcceleration(proj)  -- Check weapon blueprint for trajectory fix request

        return proj
    end


    for k, launcher in launchers do 
        local weapon = launcher:GetWeapon(1)
        weapon.CreateProjectileAtMuzzle = CreateProjectileAtMuzzle
    end
end

-- ensures that the SCU's can keep on assisting
function StrategicAssistThread(launchers) 

    while true do 

        WaitSeconds(10)

        for k, launcher in launchers do 
            if launcher:GetNukeSiloAmmoCount() > 2 then 
                launcher:RemoveNukeSiloAmmo(1)
            end 
        end 
    end 

end 

function StrategicThread(launchers)
    local count = -3
    local iteration = 0
    local size = ScenarioInfo.size

    -- picks a random axis to fire over
    local function RandomPointOnEdge()
        local axis = Random()
        local left = Random()
        if axis > 0.5 then 
            -- x axis
            if left > 0.5 then 
                return VECTOR3(10, 0, Random() * size[2])
            else 
                return VECTOR3(size[1] - 10, 0, Random() * size[2])
            end
        else
            -- y axis
            if left > 0.5 then 
                return VECTOR3(Random() * size[2], 0, 10 )
            else 
                return VECTOR3(Random() * size[2], 0, size[1] - 10)
            end
        end
    end

    -- make them launch every now and then
    while true do 
        WaitSeconds(60)

        iteration = iteration + 1
        if iteration > 2 then 
            count = count + 1 
            iteration = 0 
        end

        -- check if we want to randomly launch something
        if count > 0 then 
            for k, launcher in launchers do 
                local chance = Random() 
                if chance > 0.9 then 

                    --  LOOK HERE JAMMER
                    count = count - 1
                    launcher:GiveNukeSiloAmmo(1)
                    IssueNuke({launcher}, RandomPointOnEdge())
                    break   
                end
            end
        end
    end
end

--- Computes the center of the table of units.
function CenterOfUnits (units)
    local x = 0
    local z = 0
    local count = table.getn(units)

    for k, unit in units do 
        local position = unit:GetPosition()
        x = x + position[1]
        z = z + position[3]
    end

    x = x / count 
    z = z / count 
    y = GetSurfaceHeight(x, y)
    return VECTOR3(x, y, z)
end

-- see also lua/sim/VizMarker.lua
-- type: 'Omni', 'Radar', 'Vision', 'WaterVision'
function SetIntel(units, type )

    -- find all the players
	local armies = ListArmies();
	local players = { };
  
	for v, army in armies do
		if string.find(army, "ARMY") then
			table.insert(players, army);
		end
    end

    -- give them vision
    for k, unit in units do 
        for k, army in players do 
            unit:InitIntel(army, type, 2)
            unit:EnableIntel(type)
        end
    end
end

function AssistNearestLauncher(engineers, launchers)
    for k, engineer in engineers do 

        -- assume the first launcher is the closest
        local nearest = 1 
        local distance = 1000

        -- determine the actual closest launcher
        local pEngineer = engineer:GetPosition()
        for l, launcher in launchers do 
            local pLauncher = launcher:GetPosition()
            local dLauncher = VDist2Sq(pEngineer[1], pEngineer[3], pLauncher[1], pLauncher[3])
            if dLauncher < distance then 
                nearest = l 
                distance = dLauncher 
            end
        end

        -- assist that launcher
        IssueGuard({engineer}, launchers[nearest])
    end
end

function SetShields(units)

    local OnCollisionCheck = function(self, other)
        -- Allow strategic nuke missile to penetrate shields
        if EntityCategoryContains(categories.STRATEGIC, other) and
            EntityCategoryContains(categories.MISSILE, other) then
            return false
        end

        return true
    end

    for k, unit in units do 
        -- change the shield radius
        unit.MyShield.RegenRate = 2000
        unit.MyShield.RegenStartTime = 0
        unit.MyShield.ShieldVerticalOffset = -40
        unit.MyShield.Size = 100

        -- change the collision check
        unit.MyShield.OnCollisionCheck = OnCollisionCheck
    end
end

function DestroyUnitsThread(strArea, depth)
    while true do 

        WaitSeconds(0.25)

        local rect = ScenarioUtils.AreaToRect(strArea)
        local units = GetUnitsInRect(rect)

        if units then 
            for k, unit in units do 
                -- check if we're not already busy with this unit
                if not unit.BeingDestroyed then 
                    -- check the army
                    local army = unit:GetArmy()
                    local brain = ArmyBrains[army]
                    local name = brain.Name
                    if not (name == "NEUTRAL_CIVILIAN") then 
                        -- check the depth
                        local position = unit:GetPosition()
                        if position[2] < depth then 
                            unit.BeingDestroyed = true 
                            ForkThread(DestroyUnitThread, unit)
                        end
                    end
                end
            end
        end
    end
end

function DestroyUnitThread(unit)
    local total = unit:GetBoneCount()
    local count = 0
    -- create an explosion for each bone
    for k = 1, 2 do 
        -- don't use every bone for an explosion
        count = count + 1
        local radius = Random() * total * 0.2 + 1
        local bone = math.floor(Random() * total)
        local name = unit:GetBoneName(bone)
        DefaultExplosions.CreateDefaultHitExplosionAtBone(unit, name, radius)
    end

    unit:Destroy()
end