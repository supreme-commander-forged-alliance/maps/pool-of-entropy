## Pool of Entropy

_A map for Supreme Commander: Forged Alliance (Forever)_

![](/images/layout.png)

## Statistics of the map

![](/images/island.png)

The map is designed for a FFA or the game mode Phantom. Each player starts on its own island with a similar construct to Sludge. There are essentially no resources on the rest of the map. The islands are close enough for tactical missiles yet far away for a single t2 artillery to hit both the neighbouring islands.

There is reclaim on this map. This includes:
 - A few trees for energy.
 - A few rocks for mass.

## Civilians in the map

![](/images/center.png)

There is an underwater base in the center of the map. The base is not part of the gameplay - you cannot reclaim, capture or interact with it. Units that drop into the center will be destroyed by the shield encapsulating the base. Every now and then the base will fire a strategic missile targeting something that is outside the map. Especially when you play Phantom it will introduce a lot of tension - as each time a missile is launcher the unfamous 'Strategic Missile Launcher' audio will play.

## Techniques

![](/images/overview.png)

The latest map-development techniques are used. This includes a map-wide shadow map and normal map - significantly improving the aesthetics of the map. A timelapse of the process of making this map can be found at:
 - [Youtube: Timelapse of 'Pool of Entropy'](https://youtu.be/4oaLz1hazDw)

## License

All assets are licensed with [CC-BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).