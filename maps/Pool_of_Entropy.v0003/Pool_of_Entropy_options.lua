
options =
{
	{
		default 	= 2,
		label 		= "Rogue system state",
		help 		= "Defines whether the rogue system will arbitrarily launch a strategic missile every now and then.",
		key 		= 'PoolOfEntropyRogueSystem',
		pref	 	= 'PoolOfEntropyRogueSystem',
		values 		= {
			{ text 	= "Online", help = "The rogue system is active and will scare you every now and then.", key = 'online', },		
			{ text 	= "Offline", help = "The rogue system is not active.", key = 'offline', },	
		},	
    },
}